
import json
import requests
from flask import Flask, render_template,request, redirect, flash, url_for, session,g
from wtforms import Form, StringField, PasswordField, validators, TextAreaField,Label, SelectField, FileField
import time

app=Flask(__name__)
app.config.from_object('config')
# app.config.update(dict(
#    DEBUG=True
# ))
@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method=='POST':
        username=request.form['username']
        password=request.form['password']
        url=request.form['url']
        # KEYSTONE
        file = open('files/endpoints.json', 'w+')
        body = '{"auth": { "tenantName": "admin","passwordCredentials": {"username": "'+username+'",  "password": "'+password+'"   }}}'
        url = 'http://'+url+':5000/v2.0/tokens'
        headers = {'Content-Type': 'application/json'}
        r = requests.post(url=url, headers=headers, data=body)
        print(r.status_code)



        if(r.status_code==200):
            session['logged_in']=True
            session['username']=username
            flash('Successfully logged in','success')
            parsed = json.loads(r.text)
            novaend = parsed['access']['serviceCatalog'][0]['endpoints'][0]['adminURL']
            neutronend = parsed['access']['serviceCatalog'][1]['endpoints'][0]['adminURL']
            cinderend = parsed['access']['serviceCatalog'][2]['endpoints'][0]['adminURL']
            glanceend = parsed['access']['serviceCatalog'][9]['endpoints'][0]['adminURL']
            file.write(json.dumps(parsed, indent=4, sort_keys=True))
            file.close()
            # TOKEN
            dic = json.loads(r.text)
            token = dic['access']['token']['id']
            print(token)
            session['token']=token
            session['novaend']=novaend
            session['neutronend'] = neutronend
            session['cinderend'] = cinderend
            session['glanceend'] = glanceend
            return redirect(url_for('dashboard'))
        else:
            flash('Invalid username or password','danger')
            return redirect(url_for('login'))

    return render_template('login.html')

#DASHBOARD
@app.route("/dashboard")
def dashboard():
    return render_template('dashboard.html')

@app.route("/temp")
def temp():
    return render_template('dashboard.html')

#LOGOUT
@app.route("/logout")
def logout():
    session.clear()
    flash('You are logged out','success')
    return redirect(url_for('login'))

#HOME
@app.route("/")
def home():
    return render_template('home.html')

#IMAGES
@app.route("/image", methods=['GET'])
def img():
    if 'glanceend' in session:
        glanceend = session['glanceend']
    url = glanceend+"/v2/images"
    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}
    image = requests.get(url=url, headers=header2)
    print(image.status_code)
    print(image.text)
    return render_template('images.html', data1=image.json())

#VOLUMES
@app.route("/volume", methods=['GET'])
def vol():
    if 'cinderend' in session:
        cinderend = session['cinderend']
    url=cinderend+"/volumes/detail"
    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}
    volume=requests.get(url=url,headers=header2)
    print(volume.status_code)
    print(volume.text)
    return render_template('volumes.html', data1=volume.json())

#INSTANCES
@app.route("/instance", methods=['GET' , 'POST'])
def inst():
    if request.method == 'POST':
        # id=request.form.id
        # res = requests.delete(url=url + '/ec26bc9a-e3be-474b-ab67-5e23fe17fa83', headers=header2)
        print("delete response is")

    if 'novaend' in session:
        novaend = session['novaend']
    url = novaend+'/servers/detail'

    print(url+'/e66e7aea-f99b-4694-87c7-d0f3cd9f10ce')

    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}
    instance=requests.get(url=url,headers=header2)
    print(instance.status_code)
    print(instance.text)

    if request.method == 'POST':
        id = int(request.form['delete'])
        instance_id=instance.json()['servers'][id]['id']
        url = novaend + '/servers'
        res = requests.delete(url=url + '/'+instance_id, headers=header2)
        print("delete response is"+res.text)

    return render_template('instances.html', data1=instance.json())


#NETWORKS
@app.route("/network", methods=['GET'])
def net():
    if 'neutronend' in session:
        neutronend = session['neutronend']
    url=neutronend+"/v2.0/networks"
    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}
    net=requests.get(url=url,headers=header2)
    print(net.status_code)
    print(net.text)
    return render_template('networks.html',data1=net.json())

#CREATE INSTANCE
@app.route("/form", methods=['GET','POST'])
def reg():
    # INSTANCE FORM
    if 'neutronend' in session:
        neutronend = session['neutronend']
    url = neutronend + "/v2.0/networks"
    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}
    net = requests.get(url=url, headers=header2)
    print(net.status_code)
    print(net.text)
    dic3 = json.loads(net.text)

    if 'glanceend' in session:
        glanceend = session['glanceend']
    url = glanceend+"/v2/images"
    image = requests.get(url=url, headers=header2)
    print(image.status_code)
    print(image.text)
    dic2 = json.loads(image.text)
    class regForm(Form):
        fname = StringField('Instance Name', validators=[validators.input_required()])
        lname = SelectField(u'Flavour Ref', choices=[(1, 'm1.tiny'), (2, 'm1.small'), (3, 'm1.medium'), (4, 'm1.large'),
                                                     (5, 'm1.xlarge')], coerce=int)
        choices = [(j, dic2['images'][j]['name']) for j in range(len(dic2['images']))]
        iname = SelectField(u'Images', choices=choices, coerce=int)
        choices2 = [(j, dic3['networks'][j]['name']) for j in range(len(dic3['networks']))]
        nname = SelectField(u'Images', choices=choices2, coerce=int)
        scode = StringField('Status Code', validators=[validators.optional()])
    form=regForm(request.form)

    if request.method=='POST' and form.validate():

        name=form.fname.data
        flavour_id=form.lname.data
        i = dict(form.iname.choices).get(form.iname.data)
        imageid=''

        for k in range(len(dic2['images'])):
            if(i== dic2['images'][k]['name']):
                imageid = dic2['images'][k]['id']
                break
        i = dict(form.nname.choices).get(form.nname.data)
        networkid = ''

        for k in range(len(dic3['networks'])):
            if (i == dic3['networks'][k]['name']):
                networkid = dic3['networks'][k]['id']
                break
        print(networkid)
        if 'novaend' in session:
            novaend = session['novaend']
        url = novaend + '/servers'
        body = '{"server": {   "name": \"'+name+'\",  "imageRef": \"'+imageid+'\",   "flavorRef": \"'+str(flavour_id)+'\",    "networks": [{"uuid": \"'+networkid+'\"}]  }}'
        svr = requests.post(url=url, data=body, headers=header2)
        print(svr.status_code)
        print(svr.text)
        form.scode.data=svr.status_code
        return render_template('form.html',form=form)
    return render_template('form.html', form=form)

#CREATE IMAGE
@app.route("/createimage", methods=['GET','POST'])
def createiamge():
    if 'glanceend' in session:
        glanceend = session['glanceend']
    url = glanceend + "/v2/images"
    if 'token' in session:
        token = session['token']
    header2 = {"X-Auth-Token": token}

    class createImage(Form):
        imagename = StringField('Image Name', validators=[validators.input_required()])
        diskformat = StringField('Disk format', validators=[validators.input_required()])
        scode = StringField('Status Code', validators=[validators.optional()])
    form=createImage(request.form)

    if request.method=='POST' and form.validate():
        if request.form['button'] == 'Create':
            name = form.imagename.data
            diskformat = form.diskformat.data
            body = '{ "container_format": "bare","disk_format": "' + diskformat + '", "name": "' + name + '"}'
            img = requests.post(url=url, data=body, headers=header2)
            print(img.status_code)
            print(img.text)
            form.scode.data = img.status_code

            time.sleep(5)
            data=request.files['inputFile'].read()
            header2 = {"X-Auth-Token": token, "Content-Type": "application/octet-stream"}
            url = glanceend + '/v2/images/' + img.json()['id']+'/file'
            print('url is '+url)
            uimage = requests.put(url=url, headers=header2, data=data)
            print(uimage.status_code)
            print(uimage.text)


        return render_template('createimage.html', form=form)
    return render_template('createimage.html', form=form)


if __name__=='__main__':
    app.run(app.config['HOST'],port=6000)